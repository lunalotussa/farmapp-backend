-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 27, 2020 at 02:48 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `farmstarter`
--

-- --------------------------------------------------------

--
-- Table structure for table `kandang`
--

CREATE TABLE `kandang` (
  `id_kandang` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kandang`
--

INSERT INTO `kandang` (`id_kandang`, `nama`) VALUES
(1, 'Kandang1'),
(3, 'Kandang2');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id_login` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_login`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `master_pakan`
--

CREATE TABLE `master_pakan` (
  `id_master_pakan` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `minimum_stock` double NOT NULL,
  `stock` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_pakan`
--

INSERT INTO `master_pakan` (`id_master_pakan`, `kode`, `nama`, `minimum_stock`, `stock`) VALUES
(1, 'TI001BDL', 'Dedek', 5, 10),
(2, 'G-11', 'gemilang edit', 40, 90);

-- --------------------------------------------------------

--
-- Table structure for table `master_vov`
--

CREATE TABLE `master_vov` (
  `id_master_vov` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `minimum_stock` double NOT NULL,
  `stock` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_vov`
--

INSERT INTO `master_vov` (`id_master_vov`, `kode`, `nama`, `minimum_stock`, `stock`) VALUES
(1, 'OB123', 'Centrol 46', 5, 10),
(2, '123', 'Antibiotik', 4, 9),
(3, 'ob1', 'Oabt coba', 40, 90);

-- --------------------------------------------------------

--
-- Table structure for table `mortalitas`
--

CREATE TABLE `mortalitas` (
  `id_mortalitas` int(11) NOT NULL,
  `id_perfomance` int(11) NOT NULL,
  `id_kandang` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `unknown` int(11) NOT NULL,
  `prolab` int(11) NOT NULL,
  `lumpuh` int(11) NOT NULL,
  `sakit` int(11) NOT NULL,
  `total_mortalitas` int(11) NOT NULL,
  `total_akhir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pakan`
--

CREATE TABLE `pakan` (
  `id_pakan` int(11) NOT NULL,
  `id_perfomance` int(11) NOT NULL,
  `id_master_pakan` int(11) NOT NULL,
  `id_kandang` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `pemakaian` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `perfomance`
--

CREATE TABLE `perfomance` (
  `id_perfomance` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id_periode` int(11) NOT NULL,
  `id_kandang` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vov`
--

CREATE TABLE `vov` (
  `id_vov` int(11) NOT NULL,
  `id_perfomance` int(11) NOT NULL,
  `id_master_obat` int(11) NOT NULL,
  `id_kandang` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `pemakaian` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kandang`
--
ALTER TABLE `kandang`
  ADD PRIMARY KEY (`id_kandang`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indexes for table `master_pakan`
--
ALTER TABLE `master_pakan`
  ADD PRIMARY KEY (`id_master_pakan`);

--
-- Indexes for table `master_vov`
--
ALTER TABLE `master_vov`
  ADD PRIMARY KEY (`id_master_vov`);

--
-- Indexes for table `mortalitas`
--
ALTER TABLE `mortalitas`
  ADD PRIMARY KEY (`id_mortalitas`);

--
-- Indexes for table `pakan`
--
ALTER TABLE `pakan`
  ADD PRIMARY KEY (`id_pakan`);

--
-- Indexes for table `perfomance`
--
ALTER TABLE `perfomance`
  ADD PRIMARY KEY (`id_perfomance`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indexes for table `vov`
--
ALTER TABLE `vov`
  ADD PRIMARY KEY (`id_vov`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kandang`
--
ALTER TABLE `kandang`
  MODIFY `id_kandang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_pakan`
--
ALTER TABLE `master_pakan`
  MODIFY `id_master_pakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `master_vov`
--
ALTER TABLE `master_vov`
  MODIFY `id_master_vov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mortalitas`
--
ALTER TABLE `mortalitas`
  MODIFY `id_mortalitas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pakan`
--
ALTER TABLE `pakan`
  MODIFY `id_pakan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `perfomance`
--
ALTER TABLE `perfomance`
  MODIFY `id_perfomance` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id_periode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vov`
--
ALTER TABLE `vov`
  MODIFY `id_vov` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
