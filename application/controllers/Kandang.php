<?php

require APPPATH . 'libraries/REST_Controller.php';

class Kandang extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('KandangModel');
  }

  // method index untuk menampilkan semua data person menggunakan method get
  public function index_get(){
    $response = $this->KandangModel->all_kandang();
    $this->response($response);
  }

  // untuk menambah person menaggunakan method post
  public function add_post(){
    $response = $this->KandangModel->add_kandang(
        $this->post('nama')
      );
    $this->response($response);
  }

  // update data person menggunakan method put
  public function update_put(){
    $response = $this->KandangModel->update_kandang(
        $this->put('id_kandang'),
        $this->put('nama')
      );
    $this->response($response);
  }

  // hapus data person menggunakan method delete
  public function delete_delete(){
    $response = $this->KandangModel->delete_kandang(
        $this->delete('id_kandang')
      );
    $this->response($response);
  }

}

?>
