<?php

require APPPATH . 'libraries/REST_Controller.php';

class MasterPakan extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('MasterPakanModel');
  }

  // method index untuk menampilkan semua data person menggunakan method get
  public function index_get(){
    $response = $this->MasterPakanModel->all_master_pakan();
    $this->response($response);
  }

  // untuk menambah person menaggunakan method post
  public function add_post(){
    $response = $this->MasterPakanModel->add_master_pakan(
        $this->post('kode'),
        $this->post('nama'),
        $this->post('minimum_stock'),
        $this->post('stock')
      );
    $this->response($response);
  }

  // update data person menggunakan method put
  public function update_put(){
    $response = $this->MasterPakanModel->update_master_pakan(
        $this->put('id_master_pakan'),
        $this->put('kode'),
        $this->put('nama'),
        $this->put('minimum_stock'),
        $this->put('stock')
      );
    $this->response($response);
  }

  // hapus data person menggunakan method delete
  public function delete_delete(){
    $response = $this->MasterPakanModel->delete_master_pakan(
        $this->delete('id_master_pakan')
      );
    $this->response($response);
  }

}

?>
