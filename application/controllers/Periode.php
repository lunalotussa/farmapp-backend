<?php

require APPPATH . 'libraries/REST_Controller.php';

class Periode extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('PeriodeModel');
  }

  // method index untuk menampilkan semua data person menggunakan method get
  public function index_get(){
    $response = $this->PeriodeModel->all_periode();
    $this->response($response);
  }

  // untuk menambah person menaggunakan method post
  public function add_post(){
    $response = $this->PeriodeModel->add_periode(
        $this->post('nama'),
        $this->post('id_kandang')
      );
    $this->response($response);
  }

  // update data person menggunakan method put
  public function update_put(){
    $response = $this->PeriodeModel->update_periode(
        $this->put('id_periode'),
        $this->put('nama'),
        $this->put('id_kandang')
      );
    $this->response($response);
  }

  // hapus data person menggunakan method delete
 public function delete_delete(){
    $response = $this->PeriodeModel->delete_periode(
        $this->delete('id_periode')
      );
    $this->response($response);
  }

}

?>
