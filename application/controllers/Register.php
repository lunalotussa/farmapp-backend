<?php

require APPPATH . 'libraries/REST_Controller.php';

class Register extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('LoginModel');
  }

  // method index untuk menampilkan semua data person menggunakan method get
  public function index_get(){
    $response = $this->LoginModel->all_login();
    $this->response($response);
  }

  // untuk menambah person menaggunakan method post
  public function add_post(){
    $response = $this->LoginModel->add_login(
        $this->post('username'),
        $this->post('password')
      );
    $this->response($response);
  }

  // update data person menggunakan method put
  public function update_put(){
    $response = $this->LoginModel->update_login(
        $this->put('id_login'),
        $this->put('username'),
        $this->put('password')
      );
    $this->response($response);
  }

  // hapus data person menggunakan method delete
  public function delete_delete(){
    $response = $this->LoginModel->delete_login(
        $this->delete('id_login')
      );
    $this->response($response);
  }

}

?>
