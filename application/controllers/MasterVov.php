<?php

require APPPATH . 'libraries/REST_Controller.php';

class MasterVov extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('MasterVovModel');
  }

  // method index untuk menampilkan semua data person menggunakan method get
  public function index_get(){
    $response = $this->MasterVovModel->all_master_vov();
    $this->response($response);
  }

  // untuk menambah person menaggunakan method post
  public function add_post(){
    $response = $this->MasterVovModel->add_master_vov(
        $this->post('kode'),
        $this->post('nama'),
        $this->post('minimum_stock'),
        $this->post('stock')
      );
    $this->response($response);
  }

  // update data person menggunakan method put
  public function update_put(){
    $response = $this->MasterVovModel->update_master_vov(
        $this->put('id_master_vov'),
        $this->put('kode'),
        $this->put('nama'),
        $this->put('minimum_stock'),
        $this->put('stock')
      );
    $this->response($response);
  }

  // hapus data person menggunakan method delete
  public function delete_delete(){
    $response = $this->MasterVovModel->delete_master_vov(
        $this->delete('id_master_vov')
      );
    $this->response($response);
  }

}

?>
