<?php
require APPPATH . 'libraries/REST_Controller.php';
 
class Login extends REST_Controller{
   
     // construct
  	public function __construct(){
    parent::__construct();
    $this->load->model('LoginModel');
  	}

  // method index untuk menampilkan semua data person menggunakan method get
  	public function index_get(){
    $response = $this->LoginModel->all_login();
    $this->response($response);
  	}

	public function login_post()
	{
		$response = $this->LoginModel->check_login(
        $this->post('username'),
        $this->post('password')
      );
    $this->response($response);
  	}

}

		