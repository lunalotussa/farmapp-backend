<?php

// extends class Model
class KandangModel extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }

  // function untuk insert data ke tabel tb_person
  public function add_kandang($nama){

    if(empty($nama)){
      return $this->empty_response();
    }else{
      $data = array(
        "nama"=>$nama
      );

      $insert = $this->db->insert("kandang", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data kandang ditambahkan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data kandang gagal ditambahkan.';
        return $response;
      }
    }

  }

  // mengambil semua data person
  public function all_kandang(){

    $all = $this->db->get("kandang")->result();
    $response['status']=200;
    $response['error']=false;
    $response['person']=$all;
    return $response;

  }

  // // hapus data person
  public function delete_kandang($id_kandang){

    if($id_kandang == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id_kandang"=>$id_kandang
      );

      $this->db->where($where);
      $delete = $this->db->delete("kandang");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data kandang dihapus.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data kandang gagal dihapus.';
        return $response;
      }
    }

  }

  // update person
  public function update_kandang($id_kandang,$nama){

    if($id_kandang == '' || empty($nama)){
      return $this->empty_response();
    }else{
      $where = array(
        "id_kandang"=>$id_kandang
      );

      $set = array(
        "nama"=>$nama
      );

      $this->db->where($where);
      $update = $this->db->update("kandang",$set);
      if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data kandang diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data person kandang diubah.';
        return $response;
      }
    }

  }

}

?>
