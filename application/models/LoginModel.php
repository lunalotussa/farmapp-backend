<?php

// extends class Model
class LoginModel extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }

  // function untuk insert data ke tabel tb_person
  public function add_login($username,$password){

    if(empty($username) || empty($password)){
      return $this->empty_response();
    }else{
      $data = array(
        "username"=>$username,
        "password"=>$password
      );

      $insert = $this->db->insert("login", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data login ditambahkan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data login gagal ditambahkan.';
        return $response;
      }
    }

  }

  // mengambil semua data person
  public function all_login(){

    $all = $this->db->get("login")->result();
    $response['status']=200;
    $response['error']=false;
    $response['person']=$all;
    return $response;

  }

  // hapus data person
  public function delete_login($id_login){

    if($id_login == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id_login"=>$id_login
      );

      $this->db->where($where);
      $delete = $this->db->delete("login");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data login dihapus.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data login gagal dihapus.';
        return $response;
      }
    }

  }

  // update person
  public function update_login($id_login,$username,$password){

    if($id_login == '' || empty($username) || empty($password)){
      return $this->empty_response();
    }else{
      $where = array(
        "id_login"=>$id_login
      );

      $set = array(
        "username"=>$username,
        "password"=>$password
      );

      $this->db->where($where);
      $update = $this->db->update("login",$set);
      if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data login diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data login gagal diubah.';
        return $response;
      }
    }

  }

  public function check_login($username,$password){

    if(empty($username) || empty($password)){
      return $this->empty_response();
    }else{
      $data = array(
        "username"=>$username,
        "password"=>$password
      );

      $insert = $this->db->get_where("login", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data login ditemukan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data login tidak ditemukan.';
        return $response;
      }
    }

  }
}

?>
