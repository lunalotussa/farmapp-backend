<?php

// extends class Model
class MasterPakanModel extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }

  // function untuk insert data ke tabel tb_person
  public function add_master_pakan($kode,$nama,$minimum_stock,$stock){

    if(empty($kode) || empty($nama) || empty($minimum_stock) || empty($stock)){
      return $this->empty_response();
    }else{
      $data = array(
        "kode"=>$kode,
        "nama"=>$nama,
        "minimum_stock"=>$minimum_stock,
        "stock"=>$stock
      );

      $insert = $this->db->insert("master_pakan", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data master pakan ditambahkan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data master pakan gagal ditambahkan.';
        return $response;
      }
    }

  }

  // mengambil semua data person
  public function all_master_pakan(){

    $all = $this->db->get("master_pakan")->result();
    $response['status']=200;
    $response['error']=false;
    $response['person']=$all;
    return $response;

  }

  // hapus data person
  public function delete_master_pakan($id_master_pakan){

    if($id_master_pakan == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id_master_pakan"=>$id_master_pakan
      );

      $this->db->where($where);
      $delete = $this->db->delete("master_pakan");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data master pakan dihapus.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data master pakan gagal dihapus.';
        return $response;
      }
    }

  }

  // update person
  public function update_master_pakan($id_master_pakan,$kode,$nama,$minimum_stock,$stock){

    if($id_master_pakan == '' || empty($kode) || empty($nama) || empty($minimum_stock) || empty($stock)){
      return $this->empty_response();
    }else{
      $where = array(
        "id_master_pakan"=>$id_master_pakan
      );

      $set = array(
        "kode"=>$kode,
        "nama"=>$nama,
        "minimum_stock"=>$minimum_stock,
        "stock"=>$stock
      );

      $this->db->where($where);
      $update = $this->db->update("master_pakan",$set);
      if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data master pakan diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data master pakan gagal diubah.';
        return $response;
      }
    }

  }

}

?>
