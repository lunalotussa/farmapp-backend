<?php

// extends class Model
class PeriodeModel extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }

  // function untuk insert data ke tabel tb_person
  public function add_periode($nama,$id_kandang){

    if(empty($nama) || empty($id_kandang)){
      return $this->empty_response();
    }else{
      $data = array(
        "nama"=>$id_kandang,
        "id_kandang"=>$id_kandang
      );

      $insert = $this->db->insert("periode", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data Periode ditambahkan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data Periode gagal ditambahkan.';
        return $response;
      }
    }

  }

  // mengambil semua data person
  public function all_periode(){

    $all = $this->db->get("periode")->result();
    $response['status']=200;
    $response['error']=false;
    $response['person']=$all;
    return $response;

  }

  // hapus data person
  public function delete_periode($id_periode){

    if($id_periode == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id_periode"=>$id_periode
      );

      $this->db->where($where);
      $delete = $this->db->delete("periode");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data Periode dihapus.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data Periode gagal dihapus.';
        return $response;
      }
    }

  }

  // update person
  public function update_periode($id_periode,$nama,$id_kandang){

    if($id_periode == '' || empty($nama) || empty($id_kandang)){
      return $this->empty_response();
    }else{
      $where = array(
        "id_periode"=>$id_periode
      );

      $set = array(
        "nama"=>$nama,
        "id_kandang"=>$id_kandang
      );

      $this->db->where($where);
      $update = $this->db->update("periode",$set);
      if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data Periode diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data Periode gagal diubah.';
        return $response;
      }
    }

  }

}

?>
