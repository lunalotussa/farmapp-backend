<?php

// extends class Model
class MasterVovModel extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }

  // function untuk insert data ke tabel tb_person
  public function add_master_vov($kode,$nama,$minimum_stock,$stock){

    if(empty($kode) || empty($nama) || empty($minimum_stock) || empty($stock)){
      return $this->empty_response();
    }else{
      $data = array(
        "kode"=>$kode,
        "nama"=>$nama,
        "minimum_stock"=>$minimum_stock,
        "stock"=>$stock
      );

      $insert = $this->db->insert("master_vov", $data);

      if($insert){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data master Vov ditambahkan.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data master Vov gagal ditambahkan.';
        return $response;
      }
    }

  }

  // mengambil semua data person
  public function all_master_vov(){

    $all = $this->db->get("master_vov")->result();
    $response['status']=200;
    $response['error']=false;
    $response['person']=$all;
    return $response;

  }

  // hapus data person
  public function delete_master_vov($id_master_vov){

    if($id_master_vov == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id_master_vov"=>$id_master_vov
      );

      $this->db->where($where);
      $delete = $this->db->delete("master_vov");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data master Vov dihapus.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data master Vov gagal dihapus.';
        return $response;
      }
    }

  }

  // update person
  public function update_master_vov($id_master_vov,$kode,$nama,$minimum_stock,$stock){

    if($id_master_vov == '' || empty($kode) || empty($nama) || empty($minimum_stock) || empty($stock)){
      return $this->empty_response();
    }else{
      $where = array(
        "id_master_vov"=>$id_master_vov
      );

      $set = array(
        "kode"=>$kode,
        "nama"=>$nama,
        "minimum_stock"=>$minimum_stock,
        "stock"=>$stock
      );

      $this->db->where($where);
      $update = $this->db->update("master_vov",$set);
      if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data master Vov diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data master Vov gagal diubah.';
        return $response;
      }
    }

  }

}

?>
